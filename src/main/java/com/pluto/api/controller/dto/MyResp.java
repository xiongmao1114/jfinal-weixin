package com.pluto.api.controller.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





public class MyResp {
	
	private String errCode="0";
	private String message="success";
	private List items=new ArrayList();
	private Map<String, Object> itemMap=new HashMap<String, Object>();
	public String getErrCode() {
		return errCode;
	}
	public MyResp setErrCode(String errCode) {
		this.errCode = errCode;
		return this;
	}
	public String getMessage() {
		return message;
	}
	public MyResp setMessage(String message) {
		this.message = message;
		return this;
	}
	public List<Map> getItems() {
		return items;
	}
	public MyResp addItem(Map item) {	
		items.add(item);
		return this;
	}
	
	public MyResp addItems(List items) {
		this.items.addAll(items);
		return this;
	}
	public Map<String, Object> getItemMap() {
		return itemMap;
	}
	public MyResp addItemMap(String key, Object item) {
		this.itemMap.put(key, item);
		return this;
	}
	
	public static MyResp create(){
		return new MyResp();
	}
	
	
}
