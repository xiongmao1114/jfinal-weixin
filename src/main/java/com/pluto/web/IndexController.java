package com.pluto.web;

import com.jfinal.core.Controller;
import com.jfinal.kit.PropKit;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.SnsAccessToken;
import com.jfinal.weixin.sdk.api.SnsAccessTokenApi;
import com.jfinal.weixin.sdk.api.SnsApi;

public class IndexController extends Controller {
	public void index() {
		String code=getPara("code");
	
		ApiConfig apiConfig=ApiConfigKit.getApiConfig();
		
		SnsAccessToken token=SnsAccessTokenApi.getSnsAccessToken(apiConfig.getAppId(), apiConfig.getAppSecret(), code);
		System.out.println("accessToken:"+token.getAccessToken());
		ApiResult apiResult=SnsApi.getUserInfo(token.getAccessToken(), token.getOpenid());
		System.out.println(apiResult);
		setAttr("result", apiResult);
		render("/pages/index.html");
	}
}
